

const boardInformation = require('./callback1.js')
const allCards = require('./callback3')
const list= require('./callback2')

function allCardsForAllLists(thanosBoardId)
{

    if( typeof thanosBoardId!=='string')
    {
        throw new Error('invalid argument')
    }

    boardInformation(thanosBoardId,boardData=>{
        console.log(boardData)
        
        list(boardData.id,listData=>{
            console.log(listData)

            listData.forEach(element =>{
            
                allCards(element.id,cardData=>{
                    if(cardData!==undefined)
                    console.log(cardData)
                })
            
            })
        })
    })

}


module.exports=allCardsForAllLists;