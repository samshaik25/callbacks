const boards=require('./data/boards.json')


function boardInformation(boardId,cb)
{
    if(typeof cb!='function' || typeof boardId!=='string' )
     {
        throw new Error('invalid argument passed')
      }
      let board=boards.reduce((acc,curr)=>{

        if(curr.id==boardId)
        {
        acc=curr;
        }
        return acc
      },{})

  setTimeout(()=>{
        cb(board)
       
    },2000)
   
}


module.exports = boardInformation;