const lists=require('./data/lists.json')

function list(boardId,cb)
{
    
  if(typeof lists!=='object' || typeof boardId!='string' || typeof cb !='function')
    {
      throw new Error('invalid argument passed')
    }

    const keyOfList= Object.keys(lists) 

    let k=keyOfList.find(key=>(key==boardId))
    
   setTimeout(()=>{
       cb(lists[k])
  },2000)
}

module.exports=list;