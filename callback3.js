const cards=require('./data/cards.json')

function allCards(listId,cb)
{

    if(typeof cards!='object' || typeof listId!='string' || typeof cb !='function')
    {
      throw new Error('invalid argument passed')
    }
    const keysCards=Object.keys(cards)

    let cardId=keysCards.find(ele=>(ele==listId))

    setTimeout(()=>{
           cb(cards[cardId])
    },2000)
}

module.exports=allCards;
